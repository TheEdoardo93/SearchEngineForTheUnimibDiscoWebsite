package Solr;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.NamedList;

import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;


public class Indexing {

	/**
	 * Questo � il metodo main().
	 * Il metodo permette di:
	 * - instaurare una connessione tra l'applicazione Java e l'istanza di Solr;
	 * - controllare se l'istanza di Solr � attiva o non attiva;
	 *    - se Solr non � attivo, viene lanciata un'eccezione di tipo 'SolrServerException' (gestita) che indica
	 *    che Solr deve essere attivata prima di poter indicizzare dei documenti.
	 * - eliminare tutti i documenti presenti nell'indice di Solr;
	 * - indicizzare dei documenti nell'indice di Solr;
	 * - stampare tutti i documenti presenti nell'indice di Solr;
	 * - chiudere la connessione tra l'applicazione Java e l'istanza di Solr attiva;
	 *   
	 * @param String[] args
	 */
	public static void main(String[] args) {

		/* Create the connection between this Java application and Solr */
		try {
			HttpSolrClient solrServer = new HttpSolrClient("http://localhost:8983/solr/ProgettoIR-indice/");

			boolean SolrUp = controlloSolrAttivo(solrServer);

			if (SolrUp == true) {
				/*Delete all the documents indexed before into Solr*/
				eliminaDocumentiIndicizzatiInSolr(solrServer);

				/*Indexing*/
				
				ArrayList<ArrayList<String[]>> elenco_docenti = new ArrayList<>(); //elenco definito dei docenti
				
				ArrayList<String[]> docenti = riempiDocenti();

				SolrInputDocument[] risultato = aggiungiDocumentiInSolr(docenti, solrServer);

				/*Print all the documents indexed before into Solr*/		
				stampaDocumentiIndicizzatiInSolr(risultato);

				solrServer.close(); //close the connection with Solr
			} else {
				System.out.println("Attenzione: Solr non � attivo!");
			}
		} catch (SolrServerException e1) { //se Solr non � attivo
			System.out.println("Attenzione: Solr non � attivo! E' necessario attivare Solr!");
		} catch (ConnectException e2) {
			System.out.println("Eccezione: ConnectException");
		} catch (IOException e3) { //se ci sono problemi di I/O a livello basso
			System.out.println("Eccezione: IOException");
		}
	}

	/**
	 * Questo � il metodo 'controlloSolrAttivo()'.
	 * Il metodo permette di controllare, tramite un ping, se l'istanza di Solr � attiva oppure non � attiva.
	 * - nel caso in cui Solr non � attivo ('status' != OK), il metodo restituisce "false";
	 * - nel caso in cui Solr � attivo ('status' == OK), il metodo restituisce "true"; 
	 * 
	 * 
	 * @param solrServer
	 * @return risultato
	 * @throws SolrServerException
	 * @throws IOException
	 */
	private static boolean controlloSolrAttivo(HttpSolrClient solrServer) throws SolrServerException, IOException {
		boolean risultato = false;
		
		SolrPingResponse ping = solrServer.ping(); //pinga Solr
		NamedList<Object> output = ping.getResponse(); //ottengo il risultato del ping
		String risultatoPing = (String) output.get("status"); //controllo il valore restituito nel campo 'status'

		if (risultatoPing.equalsIgnoreCase("OK")) { //Solr � attivo
			risultato = true;
		} else { //Solr non � attivo
			risultato = false;
		}
		
		return risultato;
	}

	/**
	 * Questo � il metodo 'eliminaDocumentiIndicizzatiInSolr()'.
	 * Il metodo permette di eliminare tutti i documenti indicizzati nell'indice di Solr.
	 * - controlla sia se la query sia la commit sono andate a buon fine
	 * 
	 * @param HttpSolrClient solrServer
	 */
	private static void eliminaDocumentiIndicizzatiInSolr(HttpSolrClient solrServer) {
		try {
			UpdateResponse risposta = solrServer.deleteByQuery("*:*");
			NamedList output = risposta.getResponseHeader();
			int risultato = (int) output.getVal(0); //prendo il valore dello 'status' restituito

			if (risultato == 0) { //cancellazione dell'indice andata a buon fine
				UpdateResponse rispostaCommit = solrServer.commit(); //commit dell'operazione di cancellazione dell'indice
				if (rispostaCommit.getStatus() == 0) { //la commit � andata a buon fine
					System.out.println("Cancellato l'indice! \n");
				} else { //problemi nella commit
					System.out.println("Attenzione: problemi nella commit!");
				}
			} else { //cancellazione dell'indice non andata a buon fine
				System.out.println("Attenzione: problemi nella cancellazione dell'indice!");
			}
		} catch (SolrServerException e1) { //se c'� un errore sul server
			System.out.println("Eccezione! SolrServerException");
			e1.printStackTrace();
		} catch (IOException e2) { //se c'� un errore a basso livello di I/O
			System.out.println("Eccezione! IOException");
			e2.printStackTrace();
		}
	}

	private static ArrayList<String[]> riempiDocenti() {
		ArrayList<String[]> docenti = new ArrayList<String[]>();

		/*Formato del vettore String[] = id, nome, cognome, ruolo, pagina/e html, email, luogo ricevimento, ora ricevimento, insegna a Informatica?, insegna a TTC?, sezione*/
		String[] docente_mariani = new String[10];
		//docente_mariani[0] = "1"; 
		docente_mariani[0] = "Leonardo"; 
		docente_mariani[1] = "Mariani";
		docente_mariani[2] = "Professore di Seconda Fascia";
		docente_mariani[3] = "<!DOCTYPE html><html><body><h1>1-Leonardo Mariani</h1><p>Professore di Seconda Fascia</p></body></html>";
		docente_mariani[4] = "leonardo.mariani@disco.unimib.it";
		docente_mariani[5] = "ufficio 1 - piano 1";
		docente_mariani[6] = "09:00 - 11:00";
		docente_mariani[7] = "true";
		docente_mariani[8] = "true";
		docente_mariani[9] = "Personale";
		docenti.add(docente_mariani);
		System.out.println("Ho aggiunto questa risorsa:" + "\n" + docente_mariani[0] + "\n" + docente_mariani[1] + "\n" + docente_mariani[2] + "\n" + docente_mariani[3] + "\n" + docente_mariani[4] + "\n" + docente_mariani[5] + "\n" + docente_mariani[6] + "\n" + docente_mariani[7] + "\n" + docente_mariani[8] + "\n" + docente_mariani[9] + "\n");

		String[] docente_micucci = new String[10];
		//docente_micucci[0] = "2";
		docente_micucci[0] = "Daniela";
		docente_micucci[1] = "Micucci";
		docente_micucci[2] = "Ricercatore";
		docente_micucci[3] = "<!DOCTYPE html><html><body><h1>2-Daniela Micucci</h1><p>Ricercatore</p></body></html>";
		docente_micucci[4] = "daniela.micucci@disco.unimib.it";
		docente_micucci[5] = "ufficio 2 - piano 2";
		docente_micucci[6] = "11:00 - 13:00";
		docente_micucci[7] = "true";
		docente_micucci[8] = "false";
		docente_micucci[9] = "Personale";
		docenti.add(docente_micucci);
		System.out.println("Ho aggiunto questa risorsa:" + "\n" + docente_micucci[0] + "\n" + docente_micucci[1] + "\n" + docente_micucci[2] + "\n" + docente_micucci[3] + "\n" + docente_micucci[4] + "\n" + docente_micucci[5] + "\n" + docente_micucci[6] + "\n" + docente_micucci[7] + "\n" +docente_micucci[8] + "\n" + docente_micucci[9] + "\n");

		String[] docente_pastore = new String[10];
		//docente_pastore[0] = "3";
		docente_pastore[0] = "Fabrizio";
		docente_pastore[1] = "Pastore";
		docente_pastore[2] = "Ricercatore";
		docente_pastore[3] = "<!DOCTYPE html><html><body><h1>3-Fabrizio Pastore</h1><p>Ricercatore</p></body></html>";
		docente_pastore[4] = "fabrizio.pastore@disco.unimib.it";
		docente_pastore[5] = "ufficio 3 - piano 3";
		docente_pastore[6] = "13:00 - 15:00";
		docente_pastore[7] = "false";
		docente_pastore[8] = "true";
		docente_pastore[9] = "Personale";
		docenti.add(docente_pastore);
		System.out.println("Ho aggiunto questa risorsa:" + "\n" + docente_pastore[0] + "\n" + docente_pastore[1] + "\n" + docente_pastore[2] + "\n" + docente_pastore[3] + "\n" + docente_pastore[4] + "\n" + docente_pastore[5] + "\n" + docente_pastore[6] + "\n" + docente_pastore[7] + "\n" + docente_pastore[8] + "\n" + docente_pastore[9] + "\n");

		String[] docente_denaro = new String[10];
		//docente_denaro[0] = "4";
		docente_denaro[0] = "Giovanni";
		docente_denaro[1] = "Denaro";
		docente_denaro[2] = "Ricercatore";
		docente_denaro[3] = "<!DOCTYPE html><html><body><h1>4-Giovanni Denaro</h1><p>Ricercatore</p></body></html>";
		docente_denaro[4] = "giovanni.denaro@disco.unimib.it";
		docente_denaro[5] = "ufficio 4 - piano 4";
		docente_denaro[6] = "15:00 - 17:00";
		docente_denaro[7] = "false";
		docente_denaro[8] = "false";
		docente_denaro[9] = "Personale";
		docenti.add(docente_denaro);
		System.out.println("Ho aggiunto questa risorsa:" + "\n" + docente_denaro[0] + "\n" + docente_denaro[1] + "\n" + docente_denaro[2] + "\n" + docente_denaro[3] + "\n" + docente_denaro[4] + "\n" + docente_denaro[5] + "\n" + docente_denaro[6] + "\n" + docente_denaro[7] + "\n" + docente_denaro[8] + "\n" + docente_denaro[9] + "\n");

		return docenti;
	}

	/**
	 * Questo � il metodo 'stampaDocumentiIndicizzatiInSolr()'.
	 * Il metodo permette di stampare tutti i documenti presenti nell'indice di Solr.
	 * 
	 * @param SolrInputDocument[] risultato
	 */
	private static void stampaDocumentiIndicizzatiInSolr(SolrInputDocument[] risultato) {
		System.out.println("\n");
		System.out.println("Stampa dei documenti indicizzati in Solr:");
		for (int i = 0; i < risultato.length; i++) {
			System.out.println("Risultato " + (i + 1) + ":" + risultato[i] + "\n");
		}
	}

	/**
	 * Questo � il metodo 'aggiungiDocumentiInSolr()'.
	 * Il metodo permette di indicizzare dei documenti nell'indice di Solr.
	 * 
	 * @param ArrayList<String[]> docenti
	 * @param HttpSolrClient solrServer
	 * @return SolrInputDocument[] risultato
	 */
	private static SolrInputDocument[] aggiungiDocumentiInSolr(ArrayList<String[]> docenti, HttpSolrClient solrServer) {
		System.out.println("Il numero di risorse presenti nell'arrayList docenti � " + docenti.size());
		SolrInputDocument[] risultato = new SolrInputDocument[docenti.size()];

		int numeroDocumentiIndicizzati = 0; //number of documents indexed into Solr
		for (int i = 0; i < docenti.size(); i++) {
			SolrInputDocument doc = new SolrInputDocument(); // declare a document to add into Solr

			String[] docente_considerato = docenti.get(i);
			System.out.println("Risorsa considerata (nome e cognome): " + docente_considerato[0] + " " + docente_considerato[1]);

			/* Setting the values of the fields defined into schema.xml (il campo 'idDocumento' � generato automaticamente tramite UUID)*/
			doc.setField("nomeDocente", docente_considerato[0]);
			doc.setField("cognomeDocente", docente_considerato[1]);
			doc.setField("ruoloDocente", docente_considerato[2]);
			doc.setField("paginaHtmlDocente", docente_considerato[3]);
			doc.setField("emailDocente", docente_considerato[4]);
			doc.setField("luogoRicevimentoDocente", docente_considerato[5]);
			doc.setField("oraRicevimentoDocente", docente_considerato[6]);
			doc.setField("docenteInformatica", docente_considerato[7]);
			doc.setField("docenteTtc", docente_considerato[8]);
			doc.setField("sezione", docente_considerato[9]);

			try {
				solrServer.add(doc); // add the document to Solr
			} catch (SolrServerException e1) {
				System.out.println("Eccezione: SolrServerException");
				e1.printStackTrace();
			} catch (IOException e2) {
				System.out.println("Eccezione: IOException");
				e2.printStackTrace();
			}

			risultato[i] = doc;

			try {
				UpdateResponse risultatoCommit = solrServer.commit(); // commit dei documenti in Solr
				if (risultatoCommit.getStatus() != 0) { //problemi nella commit
					System.out.println("Attenzione: problemi nella commit dei documenti nell'indice!");
				} else {
					numeroDocumentiIndicizzati++;
					System.out.println("Sono stati aggiunti " + numeroDocumentiIndicizzati + " documenti in Solr!");
				}
			} catch (SolrServerException e1) {
				System.out.println("Eccezione: SolrServerException");
				e1.printStackTrace();
			} catch (IOException e2) {
				System.out.println("Eccezione: IOException");
				e2.printStackTrace();
			}
		}
		return risultato;
	}
}