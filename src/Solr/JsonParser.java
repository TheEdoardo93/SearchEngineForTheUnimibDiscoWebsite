package Solr;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {

	/* 
	 * Questo main lo lasciamo al momento per eventuali test
	 * 
	 public static void main(String [] args){
		ArrayList<ArrayList<String[]>> elenco_docenti = new ArrayList<>();
		elenco_docenti = parserFile();
	}
*/

	/**
	 * @return ArrayList<ArrayList<String[]>> elenco_docenti
	 */
	public ArrayList<ArrayList<String[]>> parserFile() {
		/*lettura del file da parsare*/
		ArrayList<ArrayList<String[]>> elenco_docenti = new ArrayList<>();
		ArrayList<String[]> persona;
		try {
			Scanner in = new Scanner(new FileReader("CrawlProva/personale.txt"));
			String json = "";
			while (in.hasNextLine()) 
				json += in.nextLine();
			elenco_docenti = parseDelJson(json);

			// stampaElencoDocenti(elenco_docenti);


		} catch (FileNotFoundException e) {
			System.out.println("Attenzione: file non trovato!");
			e.printStackTrace();
		}
		return elenco_docenti;
	}

	/**
	 * @param String json
	 * @return ArrayList<ArrayList<String[]>> elenco_docenti
	 */
	private ArrayList<ArrayList<String[]>>  parseDelJson(String json) {
		ArrayList<ArrayList<String[]>> elenco_docenti = new ArrayList<>();
		try {

			JSONObject o = new JSONObject(json);
			JSONArray array = o.getJSONArray("personale");

			for(int i =0; i< array.length(); i++){
				ArrayList<String []> personale = new ArrayList();

				JSONObject object = array.getJSONObject(i);
				String [] nome = new String [1];
				String [] cognome = new String [1];
				String [] ruolo;
				String [] html;
				String [] mail;
				String [] luogo_ricevimento = new String [1];
				String [] orario_ricevimento = new String [1];
				String [] docenteInformatica = new String[1];
				String [] docenteTTC = new String[1];


				nome[0] = object.getString("nome");
				cognome[0] = object.getString("cognome");
				JSONArray ruolo_json = object.getJSONArray("ruolo");
				JSONArray html_json = object.getJSONArray("html");
				JSONArray mail_json = object.getJSONArray("email");
				luogo_ricevimento[0] = object.getString("luogo_ricevimento");
				orario_ricevimento[0] = object.getString("orario_ricevimento");
				docenteInformatica[0] = object.getString("docenteInformatica");
				docenteTTC[0] = object.getString("docenteTTC");

				ruolo = new String[ruolo_json.length()];
				html = new String[html_json.length()];
				mail = new String[mail_json.length()];

				for(int j=0; j<ruolo.length;j++)
					ruolo[j]=ruolo_json.get(j).toString();
				for(int j=0; j<html.length;j++)
					html[j]=html_json.get(j).toString();
				for(int j=0; j<mail.length;j++)
					mail[j]=mail_json.get(j).toString();

				personale.add(nome);
				personale.add(cognome);
				personale.add(ruolo);
				personale.add(html);
				personale.add(mail);
				personale.add(luogo_ricevimento);
				personale.add(orario_ricevimento);
				personale.add(docenteInformatica);
				personale.add(docenteTTC);

				elenco_docenti.add(personale);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return elenco_docenti;
	}
	
	/**
	 * @param ArrayList<ArrayList<String[]>> elenco_docenti
	 */
	private void stampaElencoDocenti(ArrayList<ArrayList<String[]>> elenco_docenti) {
		for (int k=0; k<elenco_docenti.size();k++){
			ArrayList<String[]> personale = elenco_docenti.get(k);
			for (int i =0; i<personale.size();i++){
				String [] p = personale.get(i);
				for(int j =0; j<p.length;j++)
					System.out.println(p[j]);
			}
		}
	}
}
