package Solr;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

import java.io.IOException;

public class Searching {

	/**
	 * Questo � il metodo 'main()'.
	 * Il metodo permette di:
	 * - instaurare una connessione tra l'applicazione Java e l'istanza di Solr;
	 * - interrogare Solr definendo una query (ed i relativi parametri);
	 * - stampare i documenti ottenuti come risultato della interrogazione;
	 * - chiudere la connessione tra l'applicazione Java e l'istanza di Solr;
	 * 
	 * @param String[] args
	 */
	public static void main(String[] args) {

		try {
			/* Creare la connessione tra l'applicazione Java e Solr */
			HttpSolrClient solrServer = new HttpSolrClient("http://localhost:8983/solr/ProgettoIR-indice/");

			SolrDocumentList risultati = interrogazioneSolr(solrServer);

			if (risultati != null) { //l'interrogazione a Solr � andata a buon fine
				stampaDocumentiTrovati(risultati);
			} else {
				System.out.println("Attenzione: errore nell'interrogazione a Solr!");
			}

			solrServer.close(); //chiudere la connessione con Solr
		} catch (IOException e) { //errore di I/O
			System.out.println("Eccezione: IOException");
			e.printStackTrace();
		}

	}

	/**
	 * Questo � il metodo 'stampaDocumentiTrovati()'.
	 * Il metodo permette di stampare i documenti restituiti come risultato dell'interrogazione posta a Solr.
	 * 
	 * @param SolrInputDocument risultati
	 */
	private static void stampaDocumentiTrovati(SolrDocumentList risultati) {
		if (risultati.size() == 0) { //nessun risultato dalla query sottomessa a Solr
			System.out.println("Nessun risultato trovato!");
		} else {
			/*Stampa di tutti i documenti che matchano con la query*/
			for (int i = 0; i < risultati.size(); ++i) {
				System.out.println("Risultato " + (i+1) + "-esimo ottenuto dalla query: " + risultati.get(i));
			}
		}
	}

	/**
	 * Questo � il metodo 'interrogazioneSolr()'.
	 * Il metodo permette di definire una interrogazione (ed i relativi parametri) da sottomettere a Solr
	 * ed ottenere il risultato dell'interrogazione.
	 * 
	 * @param solrServer
	 * @return
	 */
	private static SolrDocumentList interrogazioneSolr(HttpSolrClient solrServer) {
		/* Imposto la query da sottomettere a Solr */
		SolrQuery query = new SolrQuery();
		query.setQuery("paginaHtmlDocente:Professore di Seconda Fascia"); // la query (e.g. campo:valore)
		query.addFilterQuery("nomeDocente:Leonardo","cognomeDocente:Mariani"); //filter query
		query.setFields("idDocumento, nomeDocente, cognomeDocente, ruoloDocente, paginaHtmlDocente, emailDocente, luogoRicevimentoDocente, oraRicevimentoDocente, docenteInformatica, docenteTtc, sezione"); //attributi in output (proiezione)
		query.setStart(0); //indica l'offset nell'insieme dei risultati completo
		query.set("defType", "edismax"); //query parser adottato

		/* Risposta da Solr alla query sottomessa in precedenza*/
		try {
			QueryResponse response = solrServer.query(query); //query a Solr
			SolrDocumentList results = response.getResults(); //risultati dalla query
			return results;
		} catch (SolrServerException e1) { //problemi del server
			System.out.println("Eccezione: SolrServerException durante la query a Solr");
			e1.printStackTrace();
			return null;
		} catch (IOException e2) { //problemi di I/O
			System.out.println("Eccezione: IOException durante la query a Solr");
			e2.printStackTrace();
			return null;
		}
	}

}