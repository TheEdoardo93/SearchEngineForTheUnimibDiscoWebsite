package Crawler;

import java.util.ArrayList;

public class Persona {

	String nome;
	String cognome;
	ArrayList<String> ruolo;
	ArrayList<String> html;
	ArrayList<String> email;
	String luogoRicevimento;
	String orarioRicevimento;
	boolean docInformatica;
	boolean docTTC;
	
	
	public Persona() {
		super();
		nome ="";
		cognome ="";
		ruolo = new ArrayList<>();
		html = new ArrayList<>();
		email = new ArrayList<>();
		luogoRicevimento = "";
		orarioRicevimento ="";
		docInformatica = false;
		docTTC = false;
	}
	public String getNome() {
		return nome;
	}
	public String getCognome() {
		return cognome;
	}
	public ArrayList<String> getRuolo() {
		return ruolo;
	}
	public ArrayList<String> getHtml() {
		return html;
	}
	public ArrayList<String> getEmail() {
		return email;
	}
	public String getLuogoRicevimento() {
		return luogoRicevimento;
	}
	public String getOrarioRicevimento() {
		return orarioRicevimento;
	}
	public boolean isDocInformatica() {
		return docInformatica;
	}
	public boolean isDocTTC() {
		return docTTC;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public void addRuolo(String ruolo) {
		this.ruolo.add(ruolo);
	}
	
	public void addHtml(String html){
		this.html.add(html);
	}
	
	public void addEmail(String email) {
		this.email.add(email);
	}
	public void setLuogoRicevimento(String luogoRicevimento) {
		this.luogoRicevimento = luogoRicevimento;
	}
	public void setOrarioRicevimento(String orarioRicevimento) {
		this.orarioRicevimento = orarioRicevimento;
	}
	public void setDocInformatica(boolean docInformatica) {
		this.docInformatica = docInformatica;
	}
	public void setDocTTC(boolean docTTC) {
		this.docTTC = docTTC;
	}
	
	
}
