package Crawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/**
 * 
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

/**
 * 
 * This class implement a controller class which specifies the seeds of the crawl,
 * the folder in which intermediate crawl data should be stored and
 * the number of concurrent threads.
 *
 */
public class Controller {
	
	private static final Logger logger = LoggerFactory.getLogger(Controller.class);
	
    public static void main(String[] args) throws Exception {
    	
    	/*
    	if (args.length != 2) {
            logger.info("Needed parameters: ");
            logger.info("\t rootFolder (it will contain intermediate crawl data)");
            logger.info("\t numberOfCralwers (number of concurrent threads)");
            return;
        }
    	
    	String crawlStorageFolder = args[0];
    	int numberOfCrawlers = Integer.parseInt(args[1]);
    	*/
    	
        String crawlStorageFolder = "./data/crawl/root"; //crawlStorageFolder is a folder where intermediate crawl data is stored.
        int numberOfCrawlers = 7; //numberOfCrawlers shows the number of concurrent threads that should be initiated for crawling.

        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(crawlStorageFolder);

        config.setPolitenessDelay(1000); //Be polite: Make sure that we don't send more than 1 request per second (1000 milliseconds between requests).
        config.setMaxDepthOfCrawling(2); //You can set the maximum crawl depth here. The default value is -1 for unlimited depth.
        config.setMaxPagesToFetch(1000); //You can set the maximum number of pages to crawl. The default value is -1 for unlimited number of pages.
        config.setIncludeBinaryContentInCrawling(false); //Do you want crawler4j to crawl also binary data? e.g. the contents of pdf, or the metadata of images etc.
        config.setResumableCrawling(false); //This config parameter can be used to set your crawl to be resumable (meaning that you can resume the crawl from a previously interrupted/crashed crawl).
        
        /*Do you need to set a proxy? If so, you can use:
            * config.setProxyHost("proxyserver.example.com");
            * config.setProxyPort(8080);
            *
            * If your proxy also needs authentication:
            * config.setProxyUsername(username); config.getProxyPassword(password);
         */
        
        /*Instantiate the controller for this crawl.*/
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);

        /* For each crawl, you need to add some seed urls. These are the first
         * URLs that are fetched and then the crawler starts following links
         * which are found in these pages.
         */
        
        //controller.addSeed("http://www.disco.unimib.it/go/Home/Italiano");
        controller.addSeed("http://www.disco.unimib.it/go/Home/Italiano/Formazione-e-Post-Laurea/Per-gli-Studenti/Ricevimento-docenti");
        controller.addSeed("http://www.disco.unimib.it/go/Home/Italiano/Personale");

        /* Start the crawl. This is a blocking operation, meaning that your code
         * will reach the line after this only when crawling is finished.
         */
        controller.start(Crawler.class, numberOfCrawlers);
    }
}
