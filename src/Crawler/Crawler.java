package Crawler;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.regex.Pattern;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * 
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

/**
 * 
 * This class decides which URLs should be crawled and handles the downloaded
 * page.
 *
 */

public class Crawler extends WebCrawler {
	private int i =0;
	private final static Pattern FILTERS = Pattern
			.compile(".*(\\.(css|js|gif|jpe?g" + "|png|mp3|tiff?|mid|mp2|mp3|mp4|zip|gz|jsp|ppt"
					+ "docx|pdf|ico|wav|avi|mov|mpeg|ram|m4v" + "|rm|smil|wmv|swf|wma|rar))$");
					//controllare docx e rss

	/**
	 * This function decides whether the given URL should be crawled or not.
	 * This method receives two parameters. The first parameter is the page in
	 * which we have discovered this new url and the second parameter is the new
	 * url. You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic). In this example,
	 * we are instructing the crawler to ignore urls that have css, js, git, ...
	 * extensions and to only accept urls that start with
	 * "http://www.ics.uci.edu/". In this case, we didn't need the referringPage
	 * parameter to make the decision.
	 */
	@Override
	public boolean shouldVisit(Page referringPage, WebURL url) {
		String href = url.getURL().toLowerCase();
		return !FILTERS.matcher(href).matches() 
				&& !(href.startsWith("http://www.disco.unimib.it/go/home/english") || href.contains("http://www.disco.unimib.it/go/home/english"))
				&& !(href.startsWith("http://www.disco.unimib.it/txt/go/home/english") || href.contains("http://www.disco.unimib.it/txt/go/home/english")) //togliere anche txt in italiano?
				&& href.startsWith("http://www.disco.unimib.it/");
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program. This function is called after the content of a URL is
	 * downloaded successfully. You can easily get the url, text, links, html,
	 * and unique id of the downloaded page.
	 */
	@Override
	public void visit(Page page) {
		String url = page.getWebURL().getURL();
		System.out.println("URL: " + url);

		if (page.getParseData() instanceof HtmlParseData) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
			String text = htmlParseData.getText();
			String html = htmlParseData.getHtml();
			Set<WebURL> links = htmlParseData.getOutgoingUrls();
			
			scriviPagina(url, html);

			System.out.println("Text length: " + text.length());
			System.out.println("Html length: " + html.length());
			System.out.println("Number of outgoing links: " + links.size());
		}
	}
	
	
	public void scriviPagina(String url, String html){
	FileWriter fileWr;
	String nomeFile = "TmpCrawl/" + Integer.toString(i);
	try {
		fileWr = new FileWriter(nomeFile);
		fileWr = new FileWriter(nomeFile, true);
		fileWr.append(url + "\n");
		fileWr.append(html);
		fileWr.flush();
		fileWr.close();			
		i++;
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
}