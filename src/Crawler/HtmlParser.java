package Crawler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HtmlParser {

	public static void main(String[] args) {
		String nomeFileRicevimento = "TmpCrawl/http___www.disco.unimib.it_go_Home_Italiano_Didattica_Per-gli-Studenti_Ricevimento-docenti.html";
		String nomeFilePersonale = "TmpCrawl/http___www.disco.unimib.it_go_Home_Italiano_Personale.html";
		Document paginaPersonale = leggiFile(nomeFilePersonale);
		Document ricDocenti = leggiFile(nomeFileRicevimento);
		ArrayList<Persona> personale = parseRicevimentoDocenti(ricDocenti);
		parsePaginaPersonale(paginaPersonale, personale);

		try {
			FileWriter fileWr = new FileWriter("CrawlProva/personale.txt", true);
			fileWr.append("{\n\"personale\":[");
			for (int i = 0; i < personale.size() - 1; i++)
				fileWr.append(scriviPersonaJSON(personale.get(i)) + ",\n");
			fileWr.append(scriviPersonaJSON(personale.get(personale.size() - 1)) + "\n");
			fileWr.append("]\n}");
			fileWr.flush();
			fileWr.close();
		} catch (IOException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static ArrayList<Persona> parseRicevimentoDocenti(Document ricDocenti) {
		Elements corpoCentroInterno = ricDocenti.select("div[class=corpoCentroInterno]");
		ArrayList<Persona> personale = new ArrayList<>();
		for (Element e : corpoCentroInterno.select("thead")) {
			Element parent = e.parent();
			for (Element t : parent.select("tbody")) {
				for (Element tr : t.select("tr")) {
					Elements td = tr.select("td");
					Persona p = new Persona();

					// nome docente[0]
					String cognome;
					if (td.get(0).select("p").isEmpty())
						cognome = td.get(0).select("strong").text();
					else
						cognome = td.get(0).select("p").select("strong").text();
					p.setCognome(cognome.replaceAll("^\\s*|\\s*$", ""));

					// cognome docente[1]
					String nome = td.get(0).html().substring(td.get(0).html().indexOf("</strong>") + 9,
							td.get(0).html().indexOf("<br>"));
					if (nome.indexOf("<a") != -1)
						nome = nome.substring(td.get(0).html().indexOf("</strong>") + 9,
								td.get(0).html().indexOf("<a"));
					p.setNome(nome.replaceAll("^\\s*|\\s*$", ""));

					// ruolo docente[2]

					// pagina html
					p.addHtml(ricDocenti.baseUri());

					// email
					String email;
					if (td.get(1).select("p").isEmpty())
						email = td.get(0).select("a").text();
					else
						email = td.get(0).select("p").select("a").text();
					p.addEmail(email.replaceAll("^\\s*|\\s*$", ""));

					// luogo ricevimento
					p.setLuogoRicevimento(td.get(1).text().replaceAll("^\\s*|\\s*$", ""));

					// orario ricevimento
					p.setOrarioRicevimento(td.get(2).text().replaceAll("^\\s", ""));

					// docente informatica
					boolean docInf = false;
					if (!td.get(3).text().replaceAll("^\\s*|\\s*$", "").isEmpty())
						docInf = true;
					p.setDocInformatica(docInf);

					// docente ttc
					boolean docTTC = false;
					if (!td.get(4).text().replaceAll("^\\s*|\\s*$", "").isEmpty())
						docTTC = true;
					p.setDocInformatica(docTTC);

					personale.add(p);

				}
			}
		}
		return personale;
	}

	private static Document leggiFile(String nomeFile) {
		BufferedReader br;
		String html = "";
		String url = "";
		try {
			br = new BufferedReader(new FileReader(nomeFile));
			url = br.readLine();
			String linea = "";
			while ((linea = br.readLine()) != null)
				html += linea;
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document doc = Jsoup.parse(html, url);
		return doc;

	}

	private static ArrayList<Persona> parsePaginaPersonale(Document paginaPersonale, ArrayList<Persona> docenti) {
		Elements corpoCentroInterno = paginaPersonale.select("div[class=corpoCentroInterno]");
		for (Element e : corpoCentroInterno) {
			for (Element db : e.select("div[class=descrBlocco]")) {
				String ruolo = "";
				for (Element dbtitolo : db.select("div[class=descrBloccoTitolo"))
					ruolo = dbtitolo.text();
				for (Element userContent : db.select("div[class=userContent"))
					for (Element ul : userContent.select("ul")) {
						for (Element li : userContent.select("li")) {
							for (Persona p : docenti) {
								if (li.text().indexOf(p.getCognome()) != -1 && li.text().indexOf(p.getNome()) != -1) {
									p.addRuolo(ruolo);
								}
							}
						}
					}
			}
		}
		return docenti;

	}

	private static String scriviPersonaJSON(Persona p) {
		String html = "";
		String email = "";
		String ruolo = "";

		if (p.getHtml().size() > 0) {
			for (int i = 0; i < p.getHtml().size(); i++)
				html += "\"" + p.getHtml().get(i).replaceAll("\"", "\\\\\"") + "\",";
			html = html.substring(0, html.length() - 1);
			html = html.replaceAll("\\n", "");
		}

		if (p.getEmail().size() > 0) {
			for (int i = 0; i < p.getEmail().size(); i++)
				email += "\"" + p.getEmail().get(i) + "\",";
			email = email.substring(0, email.length() - 1);
		}

		if (p.getRuolo().size() > 0) {
			for (int i = 0; i < p.getRuolo().size(); i++)
				ruolo += "\"" + p.getRuolo().get(i).replaceAll("\"", "\\\\\"") + "\",";
			ruolo = ruolo.substring(0, ruolo.length() - 1);
		}

		String json = "{" + "\"nome\":\"" + p.getNome() + "\",\n" + "\"cognome\":\"" + p.getCognome() + "\",\n"
				+ "\"ruolo\":[" + ruolo + "],\n" + "\"html\":[" + html + "],\n" + "\"email\":[" + email + "],\n"
				+ "\"luogo_ricevimento\":\"" + p.getLuogoRicevimento() + "\",\n" + "\"orario_ricevimento\":\""
				+ p.getOrarioRicevimento() + "\",\n" + "\"docenteInformatica\":\"" + p.isDocInformatica() + "\",\n"
				+ "\"docenteTTC\":\"" + p.isDocTTC() + "\"}";
		return json;
	}
}
